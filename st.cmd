# Startup for KG-GTA:SC-IOC-001

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")
asSetFilename("odhds_security.acf")
asSetSubstitutions("$(ASG_SET_SUBSTITUTIONS_STRINGS)")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/kg_gta_odh_plc_02.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(IOCVERSION=)")

